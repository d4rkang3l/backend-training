package com.unlimited.backend.training.servlet;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

/**
 * @author Iulian Dumitru
 */
@Builder
@Getter
public class Info {

    private String version;
    private String name;
    private long cnt;


}
