package com.unlimited.backend.training.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Iulian Dumitru
 */
public class InfoServlet extends HttpServlet {


    private ObjectMapper mapper = new ObjectMapper();

    private AtomicLong counter = new AtomicLong(0);

    @Override
    protected void doGet(HttpServletRequest req,
                         HttpServletResponse resp) throws ServletException, IOException {


        String showCnt = req.getParameter("showCnt");

        Boolean showCntBool = Boolean.parseBoolean(showCnt);


        PrintWriter writer = resp.getWriter();

        long cnt = counter.incrementAndGet();

        Info.InfoBuilder builder = Info.builder()
                .name("MagicService")
                .version("1.0");

        if (showCntBool) {
            builder.cnt(cnt);
        }

        Info info = builder.build();

        resp.addHeader("Content-Type","application/json");

        mapper.writeValue(writer, info);




    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String someParam = req.getParameter("someParam");

        System.out.println("someParam = " + someParam);


    }

}
