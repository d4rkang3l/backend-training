package com.unlimited.backend.training.servlet;

/**
 * @author Iulian Dumitru
 */
public class MathService {

    private final PiCalculator piCalculator;

    public MathService(final PiCalculator piCalculator) {
        this.piCalculator = piCalculator;
    }


    /**
     *  This method computes the mening of life.
     *  If age < 42 then the result is 0
     *
     * @param age
     * @return
     */
    public int computeMeaningOfLife(int age) {
        return 11111;
    }


    public double computeArea(int someParam) {
        return piCalculator.computePi(2) * 3.2;
    }


}
