package com.unlimited.backend.training.springboot;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Iulian Dumitru
 */
@Configuration
public class AccountServiceConfiguration {

    @Bean
    public AccountService accountService(AccountRepository accountRepository) {
        return new PlatformAccountService(accountRepository);
    }


}
