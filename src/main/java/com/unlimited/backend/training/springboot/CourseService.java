package com.unlimited.backend.training.springboot;

import com.unlimited.backend.training.springboot.exception.CourseNotFoundException;

import java.util.List;

/**
 * @author Iulian Dumitru
 */
public interface CourseService {


    List<Course> getCourses();

    Course getCourseById(Long id) throws CourseNotFoundException;


}
