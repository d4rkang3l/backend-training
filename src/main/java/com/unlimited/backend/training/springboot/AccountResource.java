package com.unlimited.backend.training.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Iulian Dumitru
 */
@RestController
public class AccountResource {

    private static final Logger LOG = LoggerFactory.getLogger(AccountResource.class);

    private final AccountService accountService;

    public AccountResource(final AccountService accountService) {
        this.accountService = accountService;
    }

    @ResponseBody
    @RequestMapping(method = GET, value = "/account", produces = APPLICATION_JSON_VALUE)
    public Account getAccount() {

        LOG.info("Getting account ...");

        return accountService.getAccountById(1L);

    }

    @ResponseBody
    @RequestMapping(method = POST, value = "/account", produces = APPLICATION_JSON_VALUE)
    public void createAccount(@RequestBody Account account) {

        LOG.info("Getting account ...");


        accountService.saveAccount(account);


    }


}
