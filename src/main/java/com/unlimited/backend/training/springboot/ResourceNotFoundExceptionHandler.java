package com.unlimited.backend.training.springboot;

import com.unlimited.backend.training.springboot.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Iulian Dumitru
 */
@ControllerAdvice
public class ResourceNotFoundExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceNotFoundExceptionHandler.class);

    @RequestMapping(produces = "application/json")
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map<String, Object> handleUncaughtException(final ResourceNotFoundException ex) {

        HashMap<String, Object> map = new HashMap<>();
        map.put("message", ex.getMessage());
        map.put("statusCode", 404);

        return map;

    }

}
