package com.unlimited.backend.training.springboot.exception;

/**
 * @author Iulian Dumitru
 */
public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
