package com.unlimited.backend.training.springboot.exception;

/**
 * @author Iulian Dumitru
 */
public class CourseNotFoundException extends ResourceNotFoundException {

    public CourseNotFoundException(String message) {
        super(message);
    }

}
