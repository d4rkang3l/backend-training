package com.unlimited.backend.training.springboot;

import org.springframework.data.repository.CrudRepository;

/**
 * @author Iulian Dumitru
 */

public interface AccountRepository extends CrudRepository<Account, Long> {

    Account findById(Long id);

    Account findByEmail(String email);

}