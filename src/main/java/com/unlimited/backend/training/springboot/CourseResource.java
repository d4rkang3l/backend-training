package com.unlimited.backend.training.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Iulian Dumitru
 */
@RestController
public class CourseResource {

    private static final Logger LOG = LoggerFactory.getLogger(CourseResource.class);

    private final CourseService courseService;

    public CourseResource(CourseService courseService) {
        this.courseService = courseService;
    }


    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/courses")
    public ResponseEntity<List<Course>> getCourseList() {

        List<Course> courses = courseService.getCourses();

        if (courses.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

//        return ResponseEntity.ok(courses);

        return new ResponseEntity<>(courses, HttpStatus.OK);

    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json", value = "/courses/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable("id") Long id) {

        LOG.info("Getting course by id {}", id);

        Course course = courseService.getCourseById(id);

        return ResponseEntity.ok(course);

    }


}
