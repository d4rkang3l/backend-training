package com.unlimited.backend.training.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @author Iulian Dumitru
 */
@SpringBootApplication
public class AccountServiceBootstrap extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(AccountServiceBootstrap.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(AccountServiceBootstrap.class, args);
    }

}
