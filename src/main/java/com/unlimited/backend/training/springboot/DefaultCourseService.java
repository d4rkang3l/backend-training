package com.unlimited.backend.training.springboot;

import com.unlimited.backend.training.springboot.exception.CourseNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Iulian Dumitru
 */
@Service
public class DefaultCourseService implements CourseService {

    private final CourseRepository courseRepository;

    @Autowired
    public DefaultCourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAllCourses();
    }

    @Override
    public Course getCourseById(Long id) throws CourseNotFoundException {
        Course course = courseRepository.findById(id);
        if (course == null) {
            throw new CourseNotFoundException("There is no course with id " + id);
        }
        return course;
    }

}
