package com.unlimited.backend.training.springboot;

/**
 * @author Iulian Dumitru
 */

public interface AccountService {

    Account getAccountById(Long id);

    void saveAccount(Account account);
}