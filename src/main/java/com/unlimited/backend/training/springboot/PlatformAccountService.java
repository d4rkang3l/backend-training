package com.unlimited.backend.training.springboot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Iulian Dumitru
 */
public class PlatformAccountService implements AccountService {


    private static final Logger LOG = LoggerFactory.getLogger(PlatformAccountService.class);

    private AccountRepository accountRepository;

    public PlatformAccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account getAccountById(Long id) {
        return accountRepository.findById(id);
    }

    @Override
    public void saveAccount(Account account) {

        LOG.info("Saving account {} ...", account);

        accountRepository.save(account);

    }

}
