package com.unlimited.backend.training.springboot;

/**
 * JPA repo config
 *
 * @author Iulian Dumitru
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@Configuration
@EnableJpaRepositories(basePackages = "com.unlimited.backend.training.springboot")
public class RepositoryConfiguration {

    @Bean
    public AccountService accountService(AccountRepository repository) {
        return new PlatformAccountService(repository);
    }


}
