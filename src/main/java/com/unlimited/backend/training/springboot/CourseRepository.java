package com.unlimited.backend.training.springboot;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author Iulian Dumitru
 */

public interface CourseRepository extends CrudRepository<Course, Long> {

    @Query("select c from Course c order by id")
    List<Course> findAllCourses();

    @Query("select c.name from Course c")
    List<String> findAllCourseNames();

    @Query("select c from Course c where c.id = :id")
    Course findById(@Param("id") Long id);

}