package com.unlimited.backend.training.springdi;

public class C {

    public int performC() {
        System.out.println("Performing C operation");
        return 2;
    }

}