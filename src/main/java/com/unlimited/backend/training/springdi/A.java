package com.unlimited.backend.training.springdi;

public class A {

    private B b;
    private C c;

    A(B b, C c) {
        this.b = b;
        this.c = c;
    }


    public int doSomething() {

        System.out.println("A is doing something ...");

        return b.performB() + c.performC();

    }


}
