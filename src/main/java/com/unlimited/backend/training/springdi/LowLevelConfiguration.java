package com.unlimited.backend.training.springdi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Iulian Dumitru
 */
@Configuration
public class LowLevelConfiguration {

    @Bean
    public B b(){
        return new B();
    }

    @Bean
    public C c(){
        return new C();
    }


}
