package com.unlimited.backend.training.springdi;

public class B {

    public int performB() {
        System.out.println("Performing B operation");

        return 1;
    }
}