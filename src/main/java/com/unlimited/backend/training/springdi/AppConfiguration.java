package com.unlimited.backend.training.springdi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Iulian Dumitru
 */
@Configuration
public class AppConfiguration {

    @Bean
    public A a(B b, C c) {
        return new A(b, c);
    }


}
