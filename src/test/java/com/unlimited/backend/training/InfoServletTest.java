package com.unlimited.backend.training;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Iulian Dumitru
 */
public class InfoServletTest {

    @Ignore
    @Test
    public void canParseBoolean() throws Exception {

        boolean ceva = Boolean.parseBoolean("ceva");

        assertTrue("Ceva s-a intamplat", ceva);

    }

    @Test
    public void cannotParseNull() throws Exception {

        boolean ceva = Boolean.parseBoolean(null);

        assertFalse(ceva);

    }


}