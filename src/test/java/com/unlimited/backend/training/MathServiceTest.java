package com.unlimited.backend.training;

import com.unlimited.backend.training.servlet.MathService;
import com.unlimited.backend.training.servlet.PiCalculator;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Iulian Dumitru
 */
@Ignore
public class MathServiceTest {


    @Test
    public void returnZeroIfAgeLessThan42() throws Exception {

        MathService mathService = new MathService(new PiCalculator());

        int meaning = mathService.computeMeaningOfLife(2);
        assertEquals("The result must be 0!", 0, meaning);


    }

    @Test
    public void checkIfSeriveReturn() throws Exception {

        PiCalculator piCalculator = mock(PiCalculator.class);
        when(
                piCalculator.computePi(anyInt())
        )
                .thenReturn(0.2);

        MathService mathService = new MathService(piCalculator);

        double v = mathService.computeArea(23);

        assertEquals(3.56, v);


    }


}