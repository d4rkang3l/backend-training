package com.unlimited.backend.training.springdi;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * @author Iulian Dumitru
 */
public class ATest {


    @Test
    public void canDOSomething() throws Exception {


        B b = Mockito.mock(B.class);
        when(
                b.performB()
        )
                .thenReturn(1);


        C c = Mockito.mock(C.class);
        when(
                c.performC()
        )
                .thenReturn(4);



        A a = new A(b, c);

        int result = a.doSomething();
        assertEquals(5, result);


    }
}